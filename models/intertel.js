'use strict';
const _ = require('lodash');
const joi = require('joi');

module.exports = {
  schema: {
    // Describe the attributes with joi here
    id: joi.string(),
    data: joi.string(),//extension
    ora: joi.string(),//extension
    timestamp: joi.date().timestamp('javascript'),//ms
    durata: joi.string(),//extension
    label: joi.string(),
    _key: joi.string()
  },
  forClient(obj) {
    // Implement outgoing transformations here
    obj = _.omit(obj, ['_id', '_rev', '_oldRev']);
    return obj;
  },
  fromClient(obj) {
    // Implement incoming transformations here
    return obj;
  }
};
