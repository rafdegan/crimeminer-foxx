'use strict';

module.context.use('/individui', require('./routes/individui'), 'individui');
module.context.use('/luoghi', require('./routes/luoghi'), 'luoghi');
module.context.use('/reati', require('./routes/reati'), 'reati');
module.context.use('/intertel', require('./routes/intertel'), 'intertel');
module.context.use('/interamb', require('./routes/interamb'), 'interamb');
module.context.use('/imputazioni', require('./routes/imputazioni'), 'imputazioni');
