'use strict';
var db = require('@arangodb').db;
const graph_module = require("@arangodb/general-graph");
const request = require('@arangodb/request');
const errors = require('@arangodb').errors;
const aql = require('@arangodb').aql;
//aeangodb ^2.
var console = require("console");
var parse = require('csv-parse');

//

/*
const DOC_NOT_FOUND = errors.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
*/


const individuo_mod = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQmNB3abdtkCD34JKdMoIX48I1tZnjXvDbA6vpp8WGQtUXD8WvdPr-QT9At3sNeGYSj-A0w4kjIjxvL/pub?gid=679415307&single=true&output=tsv';
const intercettazioneTel_mod = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vRDsUOh4mVhD-eDe9TwCF-zyxtyPPT71cIYLfAfHGp1WTwSMvRgmn6brXsY-4uNx68MfqvK3hrBxF59/pub?gid=841422676&single=true&output=tsv';

const documentCollections = [
  "Individui",
  "Luoghi",
  "Reati"
];
const edgeCollections = [
  "InterTel",
  "InterAmb",
  "Imputazioni"
];



for (const localName of documentCollections) {
  const qualifiedName = module.context.collectionName(localName);
  //OK
  db._drop(qualifiedName);
  if (!db._collection(qualifiedName)) {
    //db._createDocumentCollection(qualifiedName);
  } else if (module.context.isProduction) {
    console.debug("collection ${qualifiedName} already exists. Leaving it untouched.");
  }
}

for (const localName of edgeCollections) {
  const qualifiedName = module.context.collectionName(localName);
  //OK
  db._drop(qualifiedName);
  if (!db._collection(qualifiedName)) {
    //db._createEdgeCollection(qualifiedName);
  } else if (module.context.isProduction) {
    console.debug("collection ${qualifiedName} already exists. Leaving it untouched.");
  }
}


//Ok
graph_module._list().forEach(g=>{

});



graph_module._drop("myGraph");
//--OK
console.debug("create");
var rel1 = graph_module._relation(module.context.collectionName(edgeCollections[0]),
    module.context.collectionName(documentCollections[0]),
    module.context.collectionName(documentCollections[0]));

var rel2 = graph_module._relation(module.context.collectionName(edgeCollections[1]),
    module.context.collectionName(documentCollections[0]),
    module.context.collectionName(documentCollections[1]));

var rel3 = graph_module._relation(module.context.collectionName(edgeCollections[2]),
    module.context.collectionName(documentCollections[0]),
    module.context.collectionName(documentCollections[2]));

var edgedefinitions = graph_module._edgeDefinitions(rel1, rel2, rel3);

var graph = graph_module._create("myGraph", edgedefinitions);


/*
var any1 = graph[module.context.collectionName(documentCollections[0])].save ({ nome : "Raf", cognome : "Ara",  _key : "yd" });
var any2 = graph[module.context.collectionName(documentCollections[0])].save ({ nome : "Pas", cognome : "Ara",  _key : "flo" });

graph[module.context.collectionName(edgeCollections[0])].save(`${module.context.collectionName(documentCollections[0])}/yd`,
`${module.context.collectionName(documentCollections[0])}/flo`,
{data: "0/0/0", _key: "0"});
*/


load('Individui');
//load('InterTel');













function load(docName){

    var parser = parse({columns: true, delimiter: '\t'});
    parser.on('error', function(err){
        console.error(err.message);
    });
    parser.on('finish', function(){
        console.log('done');
    });

    var record;
    switch(docName) {
        case 'Individui':
            request.get(individuo_mod, {timeout:5000},function (error, response, body) {
                if (!error && response.statusCode == 200) {

                    parser.on('readable', function(){
                        while(record = parser.read()){
                            console.log(record);
                            //output.push(record);
                            graph[module.context.collectionName(documentCollections[0])].save ({nome : record['NOME'], cognome : record['COGNOME'], _key : record['NEW ID']});

                        }
                    });
                    parser.write(body);
                }
            });
            break;
        case 'InterTel':
            request.get(intercettazioneTel_mod, function (error, response, body) {
                if (!error && response.statusCode == 200) {

                    parser.on('readable', function(){
                        while(record = parser.read()){
                            console.log(record);
                            //output.push(record);
                            graph[module.context.collectionName(edgeCollections[0])].save(`${module.context.collectionName(documentCollections[0])}/${record['SOURCE']}`,
                                `${module.context.collectionName(documentCollections[0])}/${record['TARGET']}`,
                                {data: record['data'], _key: record['NEW ID']});
                        }
                    });
                    parser.write(body);
                }
            });
            break;
        default:break;
    }
}


