'use strict';
const db = require('@arangodb').db;
const collections = [
  "Individui",
  "Luoghi",
  "Reati",
  "InterTel",
  "InterAmb",
  "Imputazioni"
];

for (const localName of collections) {
  const qualifiedName = module.context.collectionName(localName);
  db._drop(qualifiedName);
}
