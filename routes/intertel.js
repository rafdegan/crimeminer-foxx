'use strict';
const dd = require('dedent');
const joi = require('joi');
const httpError = require('http-errors');
const status = require('statuses');
const errors = require('@arangodb').errors;
const createRouter = require('@arangodb/foxx/router');
const InterTel = require('../models/intertel');

const InterTelItems = module.context.collection('InterTel');
const keySchema = joi.string().required()
.description('The key of the interTel');

const ARANGO_NOT_FOUND = errors.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
const ARANGO_DUPLICATE = errors.ERROR_ARANGO_UNIQUE_CONSTRAINT_VIOLATED.code;
const ARANGO_CONFLICT = errors.ERROR_ARANGO_CONFLICT.code;
const HTTP_NOT_FOUND = status('not found');
const HTTP_CONFLICT = status('conflict');

const router = createRouter();
module.exports = router;


router.tag('interTel');


const NewInterTel = Object.assign({}, InterTel, {
  schema: Object.assign({}, InterTel.schema, {
    _from: joi.string(),
    _to: joi.string()
  })
});


router.get(function (req, res) {
  res.send(InterTelItems.all());
}, 'list')
.response([InterTel], 'A list of InterTelItems.')
.summary('List all InterTelItems')
.description(dd`
  Retrieves a list of all InterTelItems.
`);


router.post(function (req, res) {
  const interTel = req.body;
  let meta;
  try {
    meta = InterTelItems.save(interTel._from, interTel._to, interTel);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_DUPLICATE) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(interTel, meta);
  res.status(201);
  res.set('location', req.makeAbsolute(
    req.reverse('detail', {key: interTel._key})
  ));
  res.send(interTel);
}, 'create')
.body(NewInterTel, 'The interTel to create.')
.response(201, InterTel, 'The created interTel.')
.error(HTTP_CONFLICT, 'The interTel already exists.')
.summary('Create a new interTel')
.description(dd`
  Creates a new interTel from the request body and
  returns the saved document.
`);


router.get(':key', function (req, res) {
  const key = req.pathParams.key;
  let interTel
  try {
    interTel = InterTelItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
  res.send(interTel);
}, 'detail')
.pathParam('key', keySchema)
.response(InterTel, 'The interTel.')
.summary('Fetch a interTel')
.description(dd`
  Retrieves a interTel by its key.
`);


router.put(':key', function (req, res) {
  const key = req.pathParams.key;
  const interTel = req.body;
  let meta;
  try {
    meta = InterTelItems.replace(key, interTel);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(interTel, meta);
  res.send(interTel);
}, 'replace')
.pathParam('key', keySchema)
.body(InterTel, 'The data to replace the interTel with.')
.response(InterTel, 'The new interTel.')
.summary('Replace a interTel')
.description(dd`
  Replaces an existing interTel with the request body and
  returns the new document.
`);


router.patch(':key', function (req, res) {
  const key = req.pathParams.key;
  const patchData = req.body;
  let interTel;
  try {
    InterTelItems.update(key, patchData);
    interTel = InterTelItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  res.send(interTel);
}, 'update')
.pathParam('key', keySchema)
.body(joi.object().description('The data to update the interTel with.'))
.response(InterTel, 'The updated interTel.')
.summary('Update a interTel')
.description(dd`
  Patches a interTel with the request body and
  returns the updated document.
`);


router.delete(':key', function (req, res) {
  const key = req.pathParams.key;
  try {
    InterTelItems.remove(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
}, 'delete')
.pathParam('key', keySchema)
.response(null)
.summary('Remove a interTel')
.description(dd`
  Deletes a interTel from the database.
`);
