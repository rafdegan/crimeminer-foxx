'use strict';
const dd = require('dedent');
const joi = require('joi');
const httpError = require('http-errors');
const status = require('statuses');
const errors = require('@arangodb').errors;
const createRouter = require('@arangodb/foxx/router');
const Reati = require('../models/reati');

const ReatiItems = module.context.collection('Reati');
const keySchema = joi.string().required()
.description('The key of the reati');

const ARANGO_NOT_FOUND = errors.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
const ARANGO_DUPLICATE = errors.ERROR_ARANGO_UNIQUE_CONSTRAINT_VIOLATED.code;
const ARANGO_CONFLICT = errors.ERROR_ARANGO_CONFLICT.code;
const HTTP_NOT_FOUND = status('not found');
const HTTP_CONFLICT = status('conflict');

const router = createRouter();
module.exports = router;


router.tag('reati');


router.get(function (req, res) {
  res.send(ReatiItems.all());
}, 'list')
.response([Reati], 'A list of ReatiItems.')
.summary('List all ReatiItems')
.description(dd`
  Retrieves a list of all ReatiItems.
`);


router.post(function (req, res) {
  const reati = req.body;
  let meta;
  try {
    meta = ReatiItems.save(reati);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_DUPLICATE) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(reati, meta);
  res.status(201);
  res.set('location', req.makeAbsolute(
    req.reverse('detail', {key: reati._key})
  ));
  res.send(reati);
}, 'create')
.body(Reati, 'The reati to create.')
.response(201, Reati, 'The created reati.')
.error(HTTP_CONFLICT, 'The reati already exists.')
.summary('Create a new reati')
.description(dd`
  Creates a new reati from the request body and
  returns the saved document.
`);


router.get(':key', function (req, res) {
  const key = req.pathParams.key;
  let reati
  try {
    reati = ReatiItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
  res.send(reati);
}, 'detail')
.pathParam('key', keySchema)
.response(Reati, 'The reati.')
.summary('Fetch a reati')
.description(dd`
  Retrieves a reati by its key.
`);


router.put(':key', function (req, res) {
  const key = req.pathParams.key;
  const reati = req.body;
  let meta;
  try {
    meta = ReatiItems.replace(key, reati);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(reati, meta);
  res.send(reati);
}, 'replace')
.pathParam('key', keySchema)
.body(Reati, 'The data to replace the reati with.')
.response(Reati, 'The new reati.')
.summary('Replace a reati')
.description(dd`
  Replaces an existing reati with the request body and
  returns the new document.
`);


router.patch(':key', function (req, res) {
  const key = req.pathParams.key;
  const patchData = req.body;
  let reati;
  try {
    ReatiItems.update(key, patchData);
    reati = ReatiItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  res.send(reati);
}, 'update')
.pathParam('key', keySchema)
.body(joi.object().description('The data to update the reati with.'))
.response(Reati, 'The updated reati.')
.summary('Update a reati')
.description(dd`
  Patches a reati with the request body and
  returns the updated document.
`);


router.delete(':key', function (req, res) {
  const key = req.pathParams.key;
  try {
    ReatiItems.remove(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
}, 'delete')
.pathParam('key', keySchema)
.response(null)
.summary('Remove a reati')
.description(dd`
  Deletes a reati from the database.
`);
