'use strict';
const dd = require('dedent');
const joi = require('joi');
const httpError = require('http-errors');
const status = require('statuses');
const errors = require('@arangodb').errors;
const createRouter = require('@arangodb/foxx/router');
const Individui = require('../models/individui');

const IndividuiItems = module.context.collection('Individui');
const keySchema = joi.string().required()
.description('The key of the individui');

const ARANGO_NOT_FOUND = errors.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
const ARANGO_DUPLICATE = errors.ERROR_ARANGO_UNIQUE_CONSTRAINT_VIOLATED.code;
const ARANGO_CONFLICT = errors.ERROR_ARANGO_CONFLICT.code;
const HTTP_NOT_FOUND = status('not found');
const HTTP_CONFLICT = status('conflict');

const router = createRouter();
module.exports = router;


router.tag('individui');


router.get(function (req, res) {
  res.send(IndividuiItems.all());
}, 'list')
.response([Individui], 'A list of IndividuiItems.')
.summary('List all IndividuiItems')
.description(dd`
  Retrieves a list of all IndividuiItems.
`);


router.post(function (req, res) {
  const individui = req.body;
  let meta;
  try {
    meta = IndividuiItems.save(individui);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_DUPLICATE) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(individui, meta);
  res.status(201);
  res.set('location', req.makeAbsolute(
    req.reverse('detail', {key: individui._key})
  ));
  res.send(individui);
}, 'create')
.body(Individui, 'The individui to create.')
.response(201, Individui, 'The created individui.')
.error(HTTP_CONFLICT, 'The individui already exists.')
.summary('Create a new individui')
.description(dd`
  Creates a new individui from the request body and
  returns the saved document.
`);


router.get(':key', function (req, res) {
  const key = req.pathParams.key;
  let individui
  try {
    individui = IndividuiItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
  res.send(individui);
}, 'detail')
.pathParam('key', keySchema)
.response(Individui, 'The individui.')
.summary('Fetch a individui')
.description(dd`
  Retrieves a individui by its key.
`);


router.put(':key', function (req, res) {
  const key = req.pathParams.key;
  const individui = req.body;
  let meta;
  try {
    meta = IndividuiItems.replace(key, individui);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(individui, meta);
  res.send(individui);
}, 'replace')
.pathParam('key', keySchema)
.body(Individui, 'The data to replace the individui with.')
.response(Individui, 'The new individui.')
.summary('Replace a individui')
.description(dd`
  Replaces an existing individui with the request body and
  returns the new document.
`);


router.patch(':key', function (req, res) {
  const key = req.pathParams.key;
  const patchData = req.body;
  let individui;
  try {
    IndividuiItems.update(key, patchData);
    individui = IndividuiItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  res.send(individui);
}, 'update')
.pathParam('key', keySchema)
.body(joi.object().description('The data to update the individui with.'))
.response(Individui, 'The updated individui.')
.summary('Update a individui')
.description(dd`
  Patches a individui with the request body and
  returns the updated document.
`);


router.delete(':key', function (req, res) {
  const key = req.pathParams.key;
  try {
    IndividuiItems.remove(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
}, 'delete')
.pathParam('key', keySchema)
.response(null)
.summary('Remove a individui')
.description(dd`
  Deletes a individui from the database.
`);
