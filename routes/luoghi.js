'use strict';
const dd = require('dedent');
const joi = require('joi');
const httpError = require('http-errors');
const status = require('statuses');
const errors = require('@arangodb').errors;
const createRouter = require('@arangodb/foxx/router');
const Luoghi = require('../models/luoghi');

const LuoghiItems = module.context.collection('Luoghi');
const keySchema = joi.string().required()
.description('The key of the luoghi');

const ARANGO_NOT_FOUND = errors.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
const ARANGO_DUPLICATE = errors.ERROR_ARANGO_UNIQUE_CONSTRAINT_VIOLATED.code;
const ARANGO_CONFLICT = errors.ERROR_ARANGO_CONFLICT.code;
const HTTP_NOT_FOUND = status('not found');
const HTTP_CONFLICT = status('conflict');

const router = createRouter();
module.exports = router;


router.tag('luoghi');


router.get(function (req, res) {
  res.send(LuoghiItems.all());
}, 'list')
.response([Luoghi], 'A list of LuoghiItems.')
.summary('List all LuoghiItems')
.description(dd`
  Retrieves a list of all LuoghiItems.
`);


router.post(function (req, res) {
  const luoghi = req.body;
  let meta;
  try {
    meta = LuoghiItems.save(luoghi);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_DUPLICATE) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(luoghi, meta);
  res.status(201);
  res.set('location', req.makeAbsolute(
    req.reverse('detail', {key: luoghi._key})
  ));
  res.send(luoghi);
}, 'create')
.body(Luoghi, 'The luoghi to create.')
.response(201, Luoghi, 'The created luoghi.')
.error(HTTP_CONFLICT, 'The luoghi already exists.')
.summary('Create a new luoghi')
.description(dd`
  Creates a new luoghi from the request body and
  returns the saved document.
`);


router.get(':key', function (req, res) {
  const key = req.pathParams.key;
  let luoghi
  try {
    luoghi = LuoghiItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
  res.send(luoghi);
}, 'detail')
.pathParam('key', keySchema)
.response(Luoghi, 'The luoghi.')
.summary('Fetch a luoghi')
.description(dd`
  Retrieves a luoghi by its key.
`);


router.put(':key', function (req, res) {
  const key = req.pathParams.key;
  const luoghi = req.body;
  let meta;
  try {
    meta = LuoghiItems.replace(key, luoghi);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(luoghi, meta);
  res.send(luoghi);
}, 'replace')
.pathParam('key', keySchema)
.body(Luoghi, 'The data to replace the luoghi with.')
.response(Luoghi, 'The new luoghi.')
.summary('Replace a luoghi')
.description(dd`
  Replaces an existing luoghi with the request body and
  returns the new document.
`);


router.patch(':key', function (req, res) {
  const key = req.pathParams.key;
  const patchData = req.body;
  let luoghi;
  try {
    LuoghiItems.update(key, patchData);
    luoghi = LuoghiItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  res.send(luoghi);
}, 'update')
.pathParam('key', keySchema)
.body(joi.object().description('The data to update the luoghi with.'))
.response(Luoghi, 'The updated luoghi.')
.summary('Update a luoghi')
.description(dd`
  Patches a luoghi with the request body and
  returns the updated document.
`);


router.delete(':key', function (req, res) {
  const key = req.pathParams.key;
  try {
    LuoghiItems.remove(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
}, 'delete')
.pathParam('key', keySchema)
.response(null)
.summary('Remove a luoghi')
.description(dd`
  Deletes a luoghi from the database.
`);
