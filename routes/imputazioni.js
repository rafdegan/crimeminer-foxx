'use strict';
const dd = require('dedent');
const joi = require('joi');
const httpError = require('http-errors');
const status = require('statuses');
const errors = require('@arangodb').errors;
const createRouter = require('@arangodb/foxx/router');
const Imputazioni = require('../models/imputazioni');

const ImputazioniItems = module.context.collection('Imputazioni');
const keySchema = joi.string().required()
.description('The key of the imputazioni');

const ARANGO_NOT_FOUND = errors.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
const ARANGO_DUPLICATE = errors.ERROR_ARANGO_UNIQUE_CONSTRAINT_VIOLATED.code;
const ARANGO_CONFLICT = errors.ERROR_ARANGO_CONFLICT.code;
const HTTP_NOT_FOUND = status('not found');
const HTTP_CONFLICT = status('conflict');

const router = createRouter();
module.exports = router;


router.tag('imputazioni');


const NewImputazioni = Object.assign({}, Imputazioni, {
  schema: Object.assign({}, Imputazioni.schema, {
    _from: joi.string(),
    _to: joi.string()
  })
});


router.get(function (req, res) {
  res.send(ImputazioniItems.all());
}, 'list')
.response([Imputazioni], 'A list of ImputazioniItems.')
.summary('List all ImputazioniItems')
.description(dd`
  Retrieves a list of all ImputazioniItems.
`);


router.post(function (req, res) {
  const imputazioni = req.body;
  let meta;
  try {
    meta = ImputazioniItems.save(imputazioni._from, imputazioni._to, imputazioni);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_DUPLICATE) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(imputazioni, meta);
  res.status(201);
  res.set('location', req.makeAbsolute(
    req.reverse('detail', {key: imputazioni._key})
  ));
  res.send(imputazioni);
}, 'create')
.body(NewImputazioni, 'The imputazioni to create.')
.response(201, Imputazioni, 'The created imputazioni.')
.error(HTTP_CONFLICT, 'The imputazioni already exists.')
.summary('Create a new imputazioni')
.description(dd`
  Creates a new imputazioni from the request body and
  returns the saved document.
`);


router.get(':key', function (req, res) {
  const key = req.pathParams.key;
  let imputazioni
  try {
    imputazioni = ImputazioniItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
  res.send(imputazioni);
}, 'detail')
.pathParam('key', keySchema)
.response(Imputazioni, 'The imputazioni.')
.summary('Fetch a imputazioni')
.description(dd`
  Retrieves a imputazioni by its key.
`);


router.put(':key', function (req, res) {
  const key = req.pathParams.key;
  const imputazioni = req.body;
  let meta;
  try {
    meta = ImputazioniItems.replace(key, imputazioni);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(imputazioni, meta);
  res.send(imputazioni);
}, 'replace')
.pathParam('key', keySchema)
.body(Imputazioni, 'The data to replace the imputazioni with.')
.response(Imputazioni, 'The new imputazioni.')
.summary('Replace a imputazioni')
.description(dd`
  Replaces an existing imputazioni with the request body and
  returns the new document.
`);


router.patch(':key', function (req, res) {
  const key = req.pathParams.key;
  const patchData = req.body;
  let imputazioni;
  try {
    ImputazioniItems.update(key, patchData);
    imputazioni = ImputazioniItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  res.send(imputazioni);
}, 'update')
.pathParam('key', keySchema)
.body(joi.object().description('The data to update the imputazioni with.'))
.response(Imputazioni, 'The updated imputazioni.')
.summary('Update a imputazioni')
.description(dd`
  Patches a imputazioni with the request body and
  returns the updated document.
`);


router.delete(':key', function (req, res) {
  const key = req.pathParams.key;
  try {
    ImputazioniItems.remove(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
}, 'delete')
.pathParam('key', keySchema)
.response(null)
.summary('Remove a imputazioni')
.description(dd`
  Deletes a imputazioni from the database.
`);
