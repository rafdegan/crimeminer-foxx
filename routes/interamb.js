'use strict';
const dd = require('dedent');
const joi = require('joi');
const httpError = require('http-errors');
const status = require('statuses');
const errors = require('@arangodb').errors;
const createRouter = require('@arangodb/foxx/router');
const InterAmb = require('../models/interamb');

const InterAmbItems = module.context.collection('InterAmb');
const keySchema = joi.string().required()
.description('The key of the interAmb');

const ARANGO_NOT_FOUND = errors.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
const ARANGO_DUPLICATE = errors.ERROR_ARANGO_UNIQUE_CONSTRAINT_VIOLATED.code;
const ARANGO_CONFLICT = errors.ERROR_ARANGO_CONFLICT.code;
const HTTP_NOT_FOUND = status('not found');
const HTTP_CONFLICT = status('conflict');

const router = createRouter();
module.exports = router;


router.tag('interAmb');


const NewInterAmb = Object.assign({}, InterAmb, {
  schema: Object.assign({}, InterAmb.schema, {
    _from: joi.string(),
    _to: joi.string()
  })
});


router.get(function (req, res) {
  res.send(InterAmbItems.all());
}, 'list')
.response([InterAmb], 'A list of InterAmbItems.')
.summary('List all InterAmbItems')
.description(dd`
  Retrieves a list of all InterAmbItems.
`);


router.post(function (req, res) {
  const interAmb = req.body;
  let meta;
  try {
    meta = InterAmbItems.save(interAmb._from, interAmb._to, interAmb);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_DUPLICATE) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(interAmb, meta);
  res.status(201);
  res.set('location', req.makeAbsolute(
    req.reverse('detail', {key: interAmb._key})
  ));
  res.send(interAmb);
}, 'create')
.body(NewInterAmb, 'The interAmb to create.')
.response(201, InterAmb, 'The created interAmb.')
.error(HTTP_CONFLICT, 'The interAmb already exists.')
.summary('Create a new interAmb')
.description(dd`
  Creates a new interAmb from the request body and
  returns the saved document.
`);


router.get(':key', function (req, res) {
  const key = req.pathParams.key;
  let interAmb
  try {
    interAmb = InterAmbItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
  res.send(interAmb);
}, 'detail')
.pathParam('key', keySchema)
.response(InterAmb, 'The interAmb.')
.summary('Fetch a interAmb')
.description(dd`
  Retrieves a interAmb by its key.
`);


router.put(':key', function (req, res) {
  const key = req.pathParams.key;
  const interAmb = req.body;
  let meta;
  try {
    meta = InterAmbItems.replace(key, interAmb);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  Object.assign(interAmb, meta);
  res.send(interAmb);
}, 'replace')
.pathParam('key', keySchema)
.body(InterAmb, 'The data to replace the interAmb with.')
.response(InterAmb, 'The new interAmb.')
.summary('Replace a interAmb')
.description(dd`
  Replaces an existing interAmb with the request body and
  returns the new document.
`);


router.patch(':key', function (req, res) {
  const key = req.pathParams.key;
  const patchData = req.body;
  let interAmb;
  try {
    InterAmbItems.update(key, patchData);
    interAmb = InterAmbItems.document(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    if (e.isArangoError && e.errorNum === ARANGO_CONFLICT) {
      throw httpError(HTTP_CONFLICT, e.message);
    }
    throw e;
  }
  res.send(interAmb);
}, 'update')
.pathParam('key', keySchema)
.body(joi.object().description('The data to update the interAmb with.'))
.response(InterAmb, 'The updated interAmb.')
.summary('Update a interAmb')
.description(dd`
  Patches a interAmb with the request body and
  returns the updated document.
`);


router.delete(':key', function (req, res) {
  const key = req.pathParams.key;
  try {
    InterAmbItems.remove(key);
  } catch (e) {
    if (e.isArangoError && e.errorNum === ARANGO_NOT_FOUND) {
      throw httpError(HTTP_NOT_FOUND, e.message);
    }
    throw e;
  }
}, 'delete')
.pathParam('key', keySchema)
.response(null)
.summary('Remove a interAmb')
.description(dd`
  Deletes a interAmb from the database.
`);
