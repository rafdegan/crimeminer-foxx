# crimeminer-foxx

crimeminer-arangodb

# License

Copyright (c) 2017 Raf

License: Apache2

# Arango
###install
sudo apt-get install arangodb3
sudo apt-get --purge remove arangodb3 
rm -rf /etc/arangodb3

###backup dir
/var/lib/arangodb-DATE
###start and stop
/usr/sbin/arangod
arangod --help

--configuration <string>                                    the configuration
                                                              file or 'none' 
                                                            (default: "")


--console <boolean>                                         start a JavaScrip
                                                           t emergency 
                                                              console (default:
                                                              false)
 
 
   --log.level <string...>                                     the global or 
                                                                 topic-specific 
                                                                 log level 
                                                                 (default: "info")
    
    
     --log.output <string...>                                    log destination(s
                                                                 ) (default: )


/etc/init.d/arangod start
/etc/init.d/arangod stop
sudo systemctl status arangodb
### dir services 
/var/lib/arangodb3-apps/_db/


##tool
arangosh
db._createDatabase("crimeminer-db");
/usr/bin/foxx-manager
##config
config : /etc/arangodb3
##aps
:/var/lib/arangodb3-apps/_db/
##log
/var/log/arangodb3/arangod.log

find / -name arangod
find / -name arangosh
?
 "csv": "^2.0.0",
 
no
"fast-csv": "^2.4.1",
console.setLogLevel('DEBUG');

---------------------
https://docs.arangodb.com/3.1/cookbook/AQL/MigratingGraphFunctionsTo3.html
FOR v, e IN ANY @startId edges1, edges2 RETURN DISTINCT e._id